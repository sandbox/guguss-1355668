<?php

/**
 * Views for line item reference displays.
 */

/**
 * Implements hook_views_default_views().
 */
function cookie_monster_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'test';
  $view->description = 'List of the cookies stored by Drupal';
  $view->tag = 'default';
  $view->base_table = 'cookie_monster';
  $view->human_name = 'Cookies';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Drupal Cookies';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access all views';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Footer: Entries from a PHP array: Add a cookie */
  $handler->display->display_options['footer']['add_cookie']['id'] = 'add_cookie';
  $handler->display->display_options['footer']['add_cookie']['table'] = 'cookie_monster';
  $handler->display->display_options['footer']['add_cookie']['field'] = 'add_cookie';
  $handler->display->display_options['footer']['add_cookie']['empty'] = TRUE;
  /* Field: Entries from a PHP array: Cookie Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'cookie_monster';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Entries from a PHP array: Cookie Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'cookie_monster';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['value']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['value']['alter']['external'] = 0;
  $handler->display->display_options['fields']['value']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['value']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['value']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['value']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['value']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['value']['alter']['html'] = 0;
  $handler->display->display_options['fields']['value']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['value']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['value']['hide_empty'] = 0;
  $handler->display->display_options['fields']['value']['empty_zero'] = 0;
  $handler->display->display_options['fields']['value']['hide_alter_empty'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'cookies_table_page');
  $handler->display->display_options['path'] = 'cookies';


  $views[$view->name] = $view;

  return $views;
}
