<?php

/**
 * Custom query class to read information from the $COOKIE array.
 * 
 * Our custom Query handler must be aware of some information about the current view.
 */
class cookie_monster_query_plugin extends views_plugin_query {

  /**
   * Execute the query.
   */
  function execute(&$view) {
    // Build the query with external data.
    // @TODO : change that to an extern API CALL to be able to reuse it.
    $cookie_list = array();

    foreach ($_COOKIE as $key => $value) {
      $cookie_list[] = array('name' => $key, 'value' => $value);
    }

    $view->result = $cookie_list;

    // Views expects results to be objects.
    foreach ($view->result as $key => $value) {
      $view->result[$key] = (object) $value;
    }
  }

  function add_field($table, $field, $alias = '', $params = array()) {
    // Needed to avoid an ajax error when using the field.
  }

}