<?php

/**
 * @file
 * Defines a basic field handler to display information about the cookie (name, value).
 */

/**
 * Field handler to display a basic text field.
 */
class cookie_monster_handler_field extends views_handler_field {
  
  // Used to keep field_alias synched with field name.
  function query() {    
    $this->field_alias = $this->real_field;
  }
  
}