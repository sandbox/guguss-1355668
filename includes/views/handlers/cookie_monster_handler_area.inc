<?php

/**
 * @file
 * Defines a form area handler in the footer of a View, that allows the user to add a new cookie.
 */

/**
 * Area handler to display a form to create a new Cookie.
 */
class cookie_monster_handler_area extends views_handler_area {

  function render($empty = FALSE) {
    if (!$empty) {
      $add_cookie_form = drupal_get_form('cookie_monster_form', 'add');
      
      return drupal_render($add_cookie_form);
    }

    return '';
  }
}
