<?php

/**
 * @file
 * Field handler to present a button to remove a cookie. 
 */

/**
 * Field handler to present a button to delete a Cookie.
 */
class cookie_monster_handler_field_delete extends views_handler_field {
  
  /*function construct() {
    parent::construct();
    
    // If the view doesn't have the cookie_name, I need an additional field to get it.
    //$this->additional_fields['order_id'] = 'order_id';

    //$this->real_field = 'order_id';
  }*/

  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    // At this point, the query has already been run, so we can access the results
    // in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {
      $cookie_name = $row->name;
      
      $form[$this->options['id']][$row_id] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#name' => 'delete-line-item-' . $row_id,
        '#cookie_name' => $cookie_name,
      );
    }
  }

  function views_form_submit($form, &$form_state) {
    $field_name = $this->options['id'];

    foreach (element_children($form[$field_name]) as $row_id) {
      if ($form_state['triggering_element']['#name'] == 'delete-line-item-' . $row_id) {
        setcookie($form[$field_name][$row_id]['#cookie_name'], "");
        drupal_set_message('The cookie : ' . $form[$field_name][$row_id]['#cookie_name'] . ' has been deleted.');
      }
    }
  }

}