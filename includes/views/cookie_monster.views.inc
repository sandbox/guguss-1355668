<?php
/* This file will include all the Views hook implementations. */

/**
 * Implements hook_views_plugin().
 */
function cookie_monster_views_plugins() {	
  return array(
    'module' => 'cookie_monster',
    'query' => array(
      'cookie_monster_query_plugin' => array(
        'title' => t('Query from eternal source'),
        'help' => t('Using a PHP array as data source.'),
        'handler' => 'cookie_monster_query_plugin',
        'parent' => 'views_query',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 *
 * Making the option appear as a view type requires implementing hook_views_data().
 *
 * With the field, we are defining how this item must be handled, but we have to
 * declare first the type of handler we are talking about. We may have different
 * handlers for each field, and we may use views default handlers or our custom
 * ones.
*/
function cookie_monster_views_data() {
  $data['cookie_monster']['table']['group']  = t('Entries from a PHP array');
  $data['cookie_monster']['table']['base'] = array(
    'title' => t('Entries from an array of cookies'),
    'help' => t('Read entries from a PHP array containing a list of cookies.'),
    'query class' => 'cookie_monster_query_plugin',  //Make sure that 'query class' property matches your custom query class.
  );

  //Define a cookie name field.
  $data['cookie_monster']['name'] = array(
    'title' => t('Cookie Name'),
    'help' => t('Display the name of the cookie.'),
    'field' => array(
      'handler' => 'cookie_monster_handler_field',
    ),
  );
  
  //Define a cookie value field.
  $data['cookie_monster']['value'] = array(
    'title' => t('Cookie Value'),
    'help' => t('Display the value of the cookie.'),
    'field' => array(
      'handler' => 'cookie_monster_handler_field',
    ),
  );
  
  // Define a button to delete a cookie.
  $data['cookie_monster']['delete_cookie'] = array(
    'field' => array(
      'title' => t('Delete button'),
      'help' => t('Display a button to delete a cookie.'),
      'handler' => 'cookie_monster_handler_field_delete',
    ),
  );
  
  // Define an area to display a form that creates a cookie.
  $data['cookie_monster']['add_cookie'] = array(
    'title' => t('Add a cookie'),
    'help' => t('Display a form to create a cookie.'),
    'area' => array(
      'handler' => 'cookie_monster_handler_area',
    ),
  );
  
  return $data;
}